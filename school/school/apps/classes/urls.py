# -*- coding: utf-8 -*-
from django.conf.urls import url
from school.apps.classes.views import (
    ClassList,
    ClassCreate,
    ClassDetail,
    ClassEdit,
    ClassDelete
)

urlpatterns = [
    url(r'^$', ClassList.as_view(), name='class'),
    url(r'^(?P<pk>[0-9]+)/delete/$', ClassDelete.as_view(), name='class_delete'),
    url(r'^add/$', ClassCreate.as_view(), name='class_add'),
    url(r'^(?P<pk>[0-9]+)/$', ClassDetail.as_view(), name='class_detail'),
    url(r'^(?P<pk>[0-9]+)/edit/$', ClassEdit.as_view(), name='class_edit'),
]
