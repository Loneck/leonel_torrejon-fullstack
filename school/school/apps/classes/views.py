from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView
from django.views.generic.edit import (    
    CreateView,
    UpdateView,
    DeleteView
)
from school.apps.classes.models import Class


# Create your views here.
class ClassList(ListView):
    model = Class


class ClassCreate(CreateView):
    model = Class
    fields = [
        'name',
        'section',
        'teacher',
        'head_teacher',
        'classroom'
    ]
    success_url = reverse_lazy('class')


class ClassDetail(DetailView):
    model = Class


class ClassEdit(UpdateView):
    model = Class
    fields = [
        'name',
        'section',
        'teacher',
        'head_teacher',
        'classroom'
    ]
    template_name_suffix = '_edit'
    success_url = reverse_lazy('class')


class ClassDelete(DeleteView):
    model = Class
    success_url = reverse_lazy('class')
