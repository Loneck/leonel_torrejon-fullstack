# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-04-16 19:45
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('students', '0001_initial'),
        ('classrooms', '0001_initial'),
        ('teachers', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Class',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, help_text='creation date')),
                ('updated_at', models.DateTimeField(auto_now=True, help_text='edition date', null=True)),
                ('name', models.CharField(max_length=100)),
                ('section', models.CharField(max_length=100)),
                ('classroom', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='classroom_class', to='classrooms.Classroom')),
                ('head_teacher', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='head_teacher_class', to='teachers.Teacher')),
                ('teacher', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='teacher_class', to='teachers.Teacher')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='StudentClass',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, help_text='creation date')),
                ('updated_at', models.DateTimeField(auto_now=True, help_text='edition date', null=True)),
                ('clas', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='class_class', to='classes.Class')),
                ('student', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='student_class', to='students.Student')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
