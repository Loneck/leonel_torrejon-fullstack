from django.db import models
from school.apps.base.models import BaseModel
from school.apps.teachers.models import Teacher
from school.apps.classrooms.models import Classroom
from school.apps.students.models import Student


# Create your models here.
class Class(BaseModel):
    name = models.CharField(
        max_length=100,
    )
    section = models.CharField(
        max_length=100,
    )
    teacher = models.ForeignKey(
        Teacher,
        related_name='teacher_class',
    )
    head_teacher = models.ForeignKey(
        Teacher,
        related_name='head_teacher_class',
    )
    classroom = models.ForeignKey(
        Classroom,
        related_name='classroom_class',
    )

    def __str__(self):
        return self.name


class StudentClass(BaseModel):
    clas = models.ForeignKey(
        Class,
        related_name='class_class',
    )
    student = models.ForeignKey(
        Student,
        related_name='student_class',
    )
