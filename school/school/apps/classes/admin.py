from django.contrib import admin
from school.apps.base.admin import BaseAdmin
from school.apps.classes.models import Class, StudentClass


# Register your models here.
@admin.register(Class)
class ClassAdmin(BaseAdmin):
    list_display = (
        'name',
        'section'
    )


@admin.register(StudentClass)
class StudentClassAdmin(BaseAdmin):
    list_display = (
        'clas',
        'student'
    )
