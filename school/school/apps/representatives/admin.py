from django.contrib import admin
from school.apps.base.admin import BaseAdmin
from school.apps.representatives.models import Representative


# Register your models here.
@admin.register(Representative)
class RepresentativeAdmin(BaseAdmin):
    list_display = (
        'name',
        'last_name',
        'mail'
    )
