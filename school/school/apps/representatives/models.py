from django.db import models
from school.apps.base.models import BaseModel


# Create your models here.
class Representative(BaseModel):
    name = models.CharField(
        max_length=100,
    )
    last_name = models.CharField(
        max_length=100,
    )
    phone = models.CharField(
        max_length=15,
    )
    mail = models.EmailField(
    )
    address = models.CharField(
        max_length=100,
    )

    def __str__(self):
        return '%s %s' %(
            self.name,
            self.last_name,
        )
