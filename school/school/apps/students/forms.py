# -*- coding: utf-8 -*-
from django.forms import ModelForm
from .models import Student


class StudentForm(ModelForm):
    class Meta:
        model = Student
        fields = [
            'name',
            'last_name',
            'phone',
            'mail',
            'representative',
        ]

    def __init__(self, *args, **kwargs):
        super(StudentForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['last_name'].widget.attrs['class'] = 'form-control'
        self.fields['phone'].widget.attrs['class'] = 'form-control'
        self.fields['mail'].widget.attrs['class'] = 'form-control'
        self.fields['representative'].widget.attrs['class'] = 'form-control'
