from django.contrib import admin
from school.apps.base.admin import BaseAdmin
from school.apps.students.models import Student


# Register your models here.
@admin.register(Student)
class StudentAdmin(BaseAdmin):
    list_display = (
        'name',
        'last_name',
        'mail'
    )
