from django.db import models
from school.apps.base.models import BaseModel
from school.apps.representatives.models import Representative

# Create your models here.
class Student(BaseModel):
    name = models.CharField(
        max_length=100,
    )
    last_name = models.CharField(
        max_length=100,
    )
    phone = models.CharField(
        max_length=100,
    )
    mail = models.EmailField(
    )
    representative = models.ForeignKey(
        Representative,
        related_name='representative_student',
    )

    def __str__(self):
        return '%s %s' %(
            self.name,
            self.last_name,
        )
