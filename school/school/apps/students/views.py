from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView
from django.views.generic.edit import DeleteView
from django.urls import reverse_lazy
from school.apps.students.models import Student
from school.apps.students.forms import StudentForm

# Create your views here.
class StudentList(ListView):
    model = Student


class StudentDelete(DeleteView):
    model = Student
    success_url = reverse_lazy('students')


def Student_Create(request):
    if request.method == 'POST':
        form = StudentForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('students')
    else:
        form = StudentForm()
    return render(
        request,
        'students/student_form.html', 
        {'form': form}
    )


def Student_detail(request, pk):
	student = get_object_or_404(Student, pk=pk)
	return render(
        request,
        'students/student_detail.html', 
        {'student': student}
    )


def Student_edit(request, pk):
    student = get_object_or_404(Student, pk=pk)
    if request.method == "POST":
        form = StudentForm(request.POST, instance=student)
        if form.is_valid():
            student = form.save(commit=False)
            student.save()
            return redirect('student_detail', pk=student.pk)
    else:
        form = StudentForm(instance=student)
    return render(request, 'students/student_edit.html', {'form': form})
