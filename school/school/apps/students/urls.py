# -*- coding: utf-8 -*-
from django.conf.urls import url
from school.apps.students.views import (
    StudentList,
    Student_Create,
    Student_detail,
    Student_edit,
    StudentDelete
)

urlpatterns = [
    url(r'^$', StudentList.as_view(), name='students'),
    url(r'^(?P<pk>[0-9]+)/delete/$', StudentDelete.as_view(), name='student_delete'),
    url(r'^add/$', Student_Create, name='students_add'),
    url(r'^(?P<pk>[0-9]+)/$', Student_detail, name='student_detail'),
    url(r'^(?P<pk>[0-9]+)/edit/$', Student_edit, name='student_edit'),
]
