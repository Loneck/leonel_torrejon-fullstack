# -*- coding: utf-8 -*-
from django.conf.urls import url
from school.apps.exams.views import (
    ExamList,
    ExamCreate,
    ExamDetail,
    ExamEdit,
    ExamDelete,
    ExamResultList,
    ExamResultCreate,
    ExamResultDetail,
    ExamResultEdit,
    ExamResultDelete
)

urlpatterns = [
    url(r'^$', ExamList.as_view(), name='exam'),
    url(r'^(?P<pk>[0-9]+)/delete/$', ExamDelete.as_view(), name='exam_delete'),
    url(r'^add/$', ExamCreate.as_view(), name='exam_add'),
    url(r'^(?P<pk>[0-9]+)/$', ExamDetail.as_view(), name='exam_detail'),
    url(r'^(?P<pk>[0-9]+)/edit/$', ExamEdit.as_view(), name='exam_edit'),
    url(r'^result/$', ExamResultList.as_view(), name='examresult'),
    url(r'^result/(?P<pk>[0-9]+)/delete/$', ExamResultDelete.as_view(), name='examresult_delete'),
    url(r'^result/add/$', ExamResultCreate.as_view(), name='examresult_add'),
    url(r'^result/(?P<pk>[0-9]+)/$', ExamResultDetail.as_view(), name='examresult_detail'),
    url(r'^result/(?P<pk>[0-9]+)/edit/$', ExamResultEdit.as_view(), name='examresult_edit'),
]
