from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView
from django.views.generic.edit import (    
    CreateView,
    UpdateView,
    DeleteView
)
from school.apps.exams.models import Exam, ExamResult


# Create your views here.
class ExamList(ListView):
    model = Exam


class ExamCreate(CreateView):
    model = Exam
    fields = [
        'name',
        'date',
        'type'
    ]
    success_url = reverse_lazy('exam')


class ExamDetail(DetailView):
    model = Exam


class ExamEdit(UpdateView):
    model = Exam
    fields = [
        'name',
        'date',
        'type'
    ]
    template_name_suffix = '_edit'
    success_url = reverse_lazy('exam')


class ExamDelete(DeleteView):
    model = Exam
    success_url = reverse_lazy('exam')


class ExamResultList(ListView):
    model = ExamResult


class ExamResultCreate(CreateView):
    model = ExamResult
    fields = [
        'student',
        'clas',
        'exam',
        'grade'
    ]
    success_url = reverse_lazy('examresult')


class ExamResultDetail(DetailView):
    model = ExamResult


class ExamResultEdit(UpdateView):
    model = ExamResult
    fields = [
        'student',
        'clas',
        'exam',
        'grade'
    ]
    template_name_suffix = '_edit'
    success_url = reverse_lazy('examresult')


class ExamResultDelete(DeleteView):
    model = ExamResult
    success_url = reverse_lazy('examresult')