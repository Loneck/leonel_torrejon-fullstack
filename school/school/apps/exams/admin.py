from django.contrib import admin
from school.apps.base.admin import BaseAdmin
from school.apps.exams.models import Exam, ExamResult


# Register your models here.
@admin.register(Exam)
class ExamAdmin(BaseAdmin):
    list_display = (
        'name',
        'date'
    )


@admin.register(ExamResult)
class ExamResultAdmin(BaseAdmin):
    list_display = (
        'clas',
        'exam',
        'grade'
    )
