from django.db import models
from school.apps.base.models import BaseModel
from school.apps.students.models import Student
from school.apps.classes.models import Class

from django.utils.translation import ugettext as _


# Create your models here.
class Exam(BaseModel):
    TYPE_ORAL, TYPE_WRITTED, TYPE_PRACTICAL = range(3)

    TYPE_CHOICES = (
        (TYPE_ORAL, _(u'Oral')),
        (TYPE_WRITTED, _(u'Escrita')),
        (TYPE_PRACTICAL, _(u'Practica'))
    )


    name = models.CharField(
        max_length=100,
    )
    date = models.DateTimeField(
		blank=True,
        null=True
    )
    type = models.PositiveIntegerField(
        choices=TYPE_CHOICES,
        default=TYPE_ORAL
    )

    def __str__(self):
        return self.name


class ExamResult(BaseModel):
    grade = models.FloatField(
        default=0
    )
    student = models.ForeignKey(
        Student,
        related_name='student_exam',
    )
    clas = models.ForeignKey(
        Class,
        related_name='classroom_exam',
    )
    exam = models.ForeignKey(
        Exam,
        related_name='exam_exam',
    )

    def __str__(self):
        return self.grade
