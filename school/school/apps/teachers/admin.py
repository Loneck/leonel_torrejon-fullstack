from django.contrib import admin
from school.apps.base.admin import BaseAdmin
from school.apps.teachers.models import Teacher


# Register your models here.
@admin.register(Teacher)
class TeacherAdmin(BaseAdmin):
    list_display = (
        'name',
        'last_name',
        'active'
    )
