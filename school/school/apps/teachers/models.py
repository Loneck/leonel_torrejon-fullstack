from django.db import models
from school.apps.base.models import BaseModel


# Create your models here.
class Teacher(BaseModel):
    name = models.CharField(
        max_length=100,
    )
    last_name = models.CharField(
        max_length=100,
    )    
    phone = models.CharField(
        max_length=15,
    )
    mail = models.EmailField(
    )
    active = models.BooleanField(
        default=True,
    )

    def __str__(self):
        return self.name
