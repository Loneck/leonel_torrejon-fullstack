from django.contrib import admin
from school.apps.base.admin import BaseAdmin
from school.apps.classrooms.models import Classroom


# Register your models here.
@admin.register(Classroom)
class ClassroomAdmin(BaseAdmin):
    list_display = (
        'name',
        'is_available'
    )
