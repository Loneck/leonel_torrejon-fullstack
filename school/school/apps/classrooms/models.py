from django.db import models
from school.apps.base.models import BaseModel


# Create your models here.
class Classroom(BaseModel):
    name = models.CharField(
        max_length=100,
    )
    is_available = models.BooleanField(
        default=True,
    )
    date = models.DateTimeField(
		blank=True,
        null=True
    )

    def __str__(self):
        return self.name
