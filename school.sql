/*
 Navicat Premium Data Transfer

 Source Server         : school
 Source Server Type    : SQLite
 Source Server Version : 3021000
 Source Schema         : main

 Target Server Type    : SQLite
 Target Server Version : 3021000
 File Encoding         : 65001

 Date: 19/04/2018 17:26:45
*/

PRAGMA foreign_keys = false;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS "auth_group";
CREATE TABLE "auth_group" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "name" varchar(80) NOT NULL,
  UNIQUE ("name" ASC)
);

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS "auth_group_permissions";
CREATE TABLE "auth_group_permissions" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "group_id" integer NOT NULL,
  "permission_id" integer NOT NULL,
  FOREIGN KEY ("group_id") REFERENCES "auth_group" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY ("permission_id") REFERENCES "auth_permission" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
);

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS "auth_permission";
CREATE TABLE "auth_permission" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "content_type_id" integer NOT NULL,
  "codename" varchar(100) NOT NULL,
  "name" varchar(255) NOT NULL,
  FOREIGN KEY ("content_type_id") REFERENCES "django_content_type" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
);

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO "auth_permission" VALUES (1, 1, 'add_logentry', 'Can add log entry');
INSERT INTO "auth_permission" VALUES (2, 1, 'change_logentry', 'Can change log entry');
INSERT INTO "auth_permission" VALUES (3, 1, 'delete_logentry', 'Can delete log entry');
INSERT INTO "auth_permission" VALUES (4, 2, 'add_permission', 'Can add permission');
INSERT INTO "auth_permission" VALUES (5, 2, 'change_permission', 'Can change permission');
INSERT INTO "auth_permission" VALUES (6, 2, 'delete_permission', 'Can delete permission');
INSERT INTO "auth_permission" VALUES (7, 3, 'add_group', 'Can add group');
INSERT INTO "auth_permission" VALUES (8, 3, 'change_group', 'Can change group');
INSERT INTO "auth_permission" VALUES (9, 3, 'delete_group', 'Can delete group');
INSERT INTO "auth_permission" VALUES (10, 4, 'add_user', 'Can add user');
INSERT INTO "auth_permission" VALUES (11, 4, 'change_user', 'Can change user');
INSERT INTO "auth_permission" VALUES (12, 4, 'delete_user', 'Can delete user');
INSERT INTO "auth_permission" VALUES (13, 5, 'add_contenttype', 'Can add content type');
INSERT INTO "auth_permission" VALUES (14, 5, 'change_contenttype', 'Can change content type');
INSERT INTO "auth_permission" VALUES (15, 5, 'delete_contenttype', 'Can delete content type');
INSERT INTO "auth_permission" VALUES (16, 6, 'add_session', 'Can add session');
INSERT INTO "auth_permission" VALUES (17, 6, 'change_session', 'Can change session');
INSERT INTO "auth_permission" VALUES (18, 6, 'delete_session', 'Can delete session');
INSERT INTO "auth_permission" VALUES (19, 7, 'add_class', 'Can add class');
INSERT INTO "auth_permission" VALUES (20, 7, 'change_class', 'Can change class');
INSERT INTO "auth_permission" VALUES (21, 7, 'delete_class', 'Can delete class');
INSERT INTO "auth_permission" VALUES (22, 8, 'add_studentclass', 'Can add student class');
INSERT INTO "auth_permission" VALUES (23, 8, 'change_studentclass', 'Can change student class');
INSERT INTO "auth_permission" VALUES (24, 8, 'delete_studentclass', 'Can delete student class');
INSERT INTO "auth_permission" VALUES (25, 9, 'add_classroom', 'Can add classroom');
INSERT INTO "auth_permission" VALUES (26, 9, 'change_classroom', 'Can change classroom');
INSERT INTO "auth_permission" VALUES (27, 9, 'delete_classroom', 'Can delete classroom');
INSERT INTO "auth_permission" VALUES (28, 10, 'add_exam', 'Can add exam');
INSERT INTO "auth_permission" VALUES (29, 10, 'change_exam', 'Can change exam');
INSERT INTO "auth_permission" VALUES (30, 10, 'delete_exam', 'Can delete exam');
INSERT INTO "auth_permission" VALUES (31, 11, 'add_examresult', 'Can add exam result');
INSERT INTO "auth_permission" VALUES (32, 11, 'change_examresult', 'Can change exam result');
INSERT INTO "auth_permission" VALUES (33, 11, 'delete_examresult', 'Can delete exam result');
INSERT INTO "auth_permission" VALUES (34, 12, 'add_representative', 'Can add representative');
INSERT INTO "auth_permission" VALUES (35, 12, 'change_representative', 'Can change representative');
INSERT INTO "auth_permission" VALUES (36, 12, 'delete_representative', 'Can delete representative');
INSERT INTO "auth_permission" VALUES (37, 13, 'add_student', 'Can add student');
INSERT INTO "auth_permission" VALUES (38, 13, 'change_student', 'Can change student');
INSERT INTO "auth_permission" VALUES (39, 13, 'delete_student', 'Can delete student');
INSERT INTO "auth_permission" VALUES (40, 14, 'add_teacher', 'Can add teacher');
INSERT INTO "auth_permission" VALUES (41, 14, 'change_teacher', 'Can change teacher');
INSERT INTO "auth_permission" VALUES (42, 14, 'delete_teacher', 'Can delete teacher');

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS "auth_user";
CREATE TABLE "auth_user" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "password" varchar(128) NOT NULL,
  "last_login" datetime,
  "is_superuser" bool NOT NULL,
  "first_name" varchar(30) NOT NULL,
  "last_name" varchar(30) NOT NULL,
  "email" varchar(254) NOT NULL,
  "is_staff" bool NOT NULL,
  "is_active" bool NOT NULL,
  "date_joined" datetime NOT NULL,
  "username" varchar(150) NOT NULL,
  UNIQUE ("username" ASC)
);

-- ----------------------------
-- Records of auth_user
-- ----------------------------
INSERT INTO "auth_user" VALUES (1, 'pbkdf2_sha256$36000$gTgSzqhzFYdb$0PWlOx/mTJgQD6L2R+udhH3cCRl/pPGRsDJTWCWzot4=', '2018-04-16 19:47:05.270509', 1, '', '', 'leo.torrejon@gmail.com', 1, 1, '2018-04-16 19:46:36.773932', 'loneck');

-- ----------------------------
-- Table structure for auth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS "auth_user_groups";
CREATE TABLE "auth_user_groups" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "user_id" integer NOT NULL,
  "group_id" integer NOT NULL,
  FOREIGN KEY ("user_id") REFERENCES "auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY ("group_id") REFERENCES "auth_group" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
);

-- ----------------------------
-- Table structure for auth_user_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS "auth_user_user_permissions";
CREATE TABLE "auth_user_user_permissions" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "user_id" integer NOT NULL,
  "permission_id" integer NOT NULL,
  FOREIGN KEY ("user_id") REFERENCES "auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY ("permission_id") REFERENCES "auth_permission" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
);

-- ----------------------------
-- Table structure for classes_class
-- ----------------------------
DROP TABLE IF EXISTS "classes_class";
CREATE TABLE "classes_class" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "created_at" datetime NOT NULL,
  "updated_at" datetime,
  "name" varchar(100) NOT NULL,
  "section" varchar(100) NOT NULL,
  "classroom_id" integer NOT NULL,
  "head_teacher_id" integer NOT NULL,
  "teacher_id" integer NOT NULL,
  FOREIGN KEY ("classroom_id") REFERENCES "classrooms_classroom" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY ("head_teacher_id") REFERENCES "teachers_teacher" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY ("teacher_id") REFERENCES "teachers_teacher" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
);

-- ----------------------------
-- Records of classes_class
-- ----------------------------
INSERT INTO "classes_class" VALUES (1, '2018-04-19 04:38:29.766261', '2018-04-19 16:33:56.789396', 'Programacion', 101, 1, 1, 2);
INSERT INTO "classes_class" VALUES (3, '2018-04-19 19:39:34.073184', '2018-04-19 19:39:57.541081', 'Base de Datos', 102, 2, 1, 2);

-- ----------------------------
-- Table structure for classes_studentclass
-- ----------------------------
DROP TABLE IF EXISTS "classes_studentclass";
CREATE TABLE "classes_studentclass" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "created_at" datetime NOT NULL,
  "updated_at" datetime,
  "clas_id" integer NOT NULL,
  "student_id" integer NOT NULL,
  FOREIGN KEY ("clas_id") REFERENCES "classes_class" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY ("student_id") REFERENCES "students_student" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
);

-- ----------------------------
-- Records of classes_studentclass
-- ----------------------------
INSERT INTO "classes_studentclass" VALUES (1, '2018-04-19 18:03:40.625486', '2018-04-19 18:03:40.625532', 1, 1);
INSERT INTO "classes_studentclass" VALUES (2, '2018-04-19 18:03:46.936482', '2018-04-19 18:03:46.936529', 1, 3);

-- ----------------------------
-- Table structure for classrooms_classroom
-- ----------------------------
DROP TABLE IF EXISTS "classrooms_classroom";
CREATE TABLE "classrooms_classroom" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "created_at" datetime NOT NULL,
  "updated_at" datetime,
  "name" varchar(100) NOT NULL,
  "is_available" bool NOT NULL,
  "date" datetime
);

-- ----------------------------
-- Records of classrooms_classroom
-- ----------------------------
INSERT INTO "classrooms_classroom" VALUES (1, '2018-04-19 03:17:47.822456', '2018-04-19 03:17:47.822508', 'Rinoceronte', 1, '2018-04-19 03:16:31');
INSERT INTO "classrooms_classroom" VALUES (2, '2018-04-19 04:51:53.101073', '2018-04-19 04:51:53.101118', 'Jirafa', 1, '2018-04-19 04:51:51');

-- ----------------------------
-- Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS "django_admin_log";
CREATE TABLE "django_admin_log" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "object_id" text,
  "object_repr" varchar(200) NOT NULL,
  "action_flag" smallint unsigned NOT NULL,
  "change_message" text NOT NULL,
  "content_type_id" integer,
  "user_id" integer NOT NULL,
  "action_time" datetime NOT NULL,
  FOREIGN KEY ("content_type_id") REFERENCES "django_content_type" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY ("user_id") REFERENCES "auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
);

-- ----------------------------
-- Records of django_admin_log
-- ----------------------------
INSERT INTO "django_admin_log" VALUES (1, 1, 'Representative object', 1, '[{"added": {}}]', 12, 1, '2018-04-17 20:53:42.538073');
INSERT INTO "django_admin_log" VALUES (2, 1, 'Teacher object', 1, '[{"added": {}}]', 14, 1, '2018-04-19 02:18:18.318503');
INSERT INTO "django_admin_log" VALUES (3, 1, 'Classroom object', 1, '[{"added": {}}]', 9, 1, '2018-04-19 03:17:47.832083');
INSERT INTO "django_admin_log" VALUES (4, 2, 'Felipe', 1, '[{"added": {}}]', 14, 1, '2018-04-19 04:38:05.553900');
INSERT INTO "django_admin_log" VALUES (5, 2, 'Jirafa', 1, '[{"added": {}}]', 9, 1, '2018-04-19 04:51:53.120963');
INSERT INTO "django_admin_log" VALUES (6, 1, 'StudentClass object', 1, '[{"added": {}}]', 8, 1, '2018-04-19 18:03:40.658231');
INSERT INTO "django_admin_log" VALUES (7, 2, 'StudentClass object', 1, '[{"added": {}}]', 8, 1, '2018-04-19 18:03:46.945864');
INSERT INTO "django_admin_log" VALUES (8, 4, 'Felipe Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:12:49.214670');
INSERT INTO "django_admin_log" VALUES (9, 5, 'Sofia Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:12:58.829354');
INSERT INTO "django_admin_log" VALUES (10, 6, 'Angel Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:13:11.250258');
INSERT INTO "django_admin_log" VALUES (11, 7, 'Horacio Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:13:22.675131');
INSERT INTO "django_admin_log" VALUES (12, 8, 'Fabian Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:13:48.808199');
INSERT INTO "django_admin_log" VALUES (13, 9, 'Claudio Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:13:58.680786');
INSERT INTO "django_admin_log" VALUES (14, 10, 'Haylin Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:14:06.720693');
INSERT INTO "django_admin_log" VALUES (15, 11, 'Nicolas Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:14:27.806713');
INSERT INTO "django_admin_log" VALUES (16, 12, 'Humberto Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:14:37.428817');
INSERT INTO "django_admin_log" VALUES (17, 13, 'Enrique Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:14:49.851024');
INSERT INTO "django_admin_log" VALUES (18, 14, 'Nicole Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:15:18.240425');
INSERT INTO "django_admin_log" VALUES (19, 15, 'Andrea Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:15:27.435785');
INSERT INTO "django_admin_log" VALUES (20, 16, 'Noemi Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:15:36.142724');
INSERT INTO "django_admin_log" VALUES (21, 17, 'Florentina Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:15:43.802757');
INSERT INTO "django_admin_log" VALUES (22, 18, 'Carolina Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:15:52.839879');
INSERT INTO "django_admin_log" VALUES (23, 19, 'Paola Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:16:14.642740');
INSERT INTO "django_admin_log" VALUES (24, 20, 'Armando Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:16:41.171389');
INSERT INTO "django_admin_log" VALUES (25, 21, 'Anakin Torrejon', 1, '[{"added": {}}]', 13, 1, '2018-04-19 20:16:51.568159');
INSERT INTO "django_admin_log" VALUES (26, 21, 'Anakin Torrejon', 2, '[]', 13, 1, '2018-04-19 20:16:55.344250');

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS "django_content_type";
CREATE TABLE "django_content_type" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "app_label" varchar(100) NOT NULL,
  "model" varchar(100) NOT NULL
);

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO "django_content_type" VALUES (1, 'admin', 'logentry');
INSERT INTO "django_content_type" VALUES (2, 'auth', 'permission');
INSERT INTO "django_content_type" VALUES (3, 'auth', 'group');
INSERT INTO "django_content_type" VALUES (4, 'auth', 'user');
INSERT INTO "django_content_type" VALUES (5, 'contenttypes', 'contenttype');
INSERT INTO "django_content_type" VALUES (6, 'sessions', 'session');
INSERT INTO "django_content_type" VALUES (7, 'classes', 'class');
INSERT INTO "django_content_type" VALUES (8, 'classes', 'studentclass');
INSERT INTO "django_content_type" VALUES (9, 'classrooms', 'classroom');
INSERT INTO "django_content_type" VALUES (10, 'exams', 'exam');
INSERT INTO "django_content_type" VALUES (11, 'exams', 'examresult');
INSERT INTO "django_content_type" VALUES (12, 'representatives', 'representative');
INSERT INTO "django_content_type" VALUES (13, 'students', 'student');
INSERT INTO "django_content_type" VALUES (14, 'teachers', 'teacher');

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS "django_migrations";
CREATE TABLE "django_migrations" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "app" varchar(255) NOT NULL,
  "name" varchar(255) NOT NULL,
  "applied" datetime NOT NULL
);

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO "django_migrations" VALUES (1, 'contenttypes', '0001_initial', '2018-04-16 19:46:04.587899');
INSERT INTO "django_migrations" VALUES (2, 'auth', '0001_initial', '2018-04-16 19:46:04.822389');
INSERT INTO "django_migrations" VALUES (3, 'admin', '0001_initial', '2018-04-16 19:46:05.012646');
INSERT INTO "django_migrations" VALUES (4, 'admin', '0002_logentry_remove_auto_add', '2018-04-16 19:46:05.259851');
INSERT INTO "django_migrations" VALUES (5, 'contenttypes', '0002_remove_content_type_name', '2018-04-16 19:46:05.433837');
INSERT INTO "django_migrations" VALUES (6, 'auth', '0002_alter_permission_name_max_length', '2018-04-16 19:46:05.588639');
INSERT INTO "django_migrations" VALUES (7, 'auth', '0003_alter_user_email_max_length', '2018-04-16 19:46:05.764242');
INSERT INTO "django_migrations" VALUES (8, 'auth', '0004_alter_user_username_opts', '2018-04-16 19:46:05.944946');
INSERT INTO "django_migrations" VALUES (9, 'auth', '0005_alter_user_last_login_null', '2018-04-16 19:46:06.088927');
INSERT INTO "django_migrations" VALUES (10, 'auth', '0006_require_contenttypes_0002', '2018-04-16 19:46:06.144140');
INSERT INTO "django_migrations" VALUES (11, 'auth', '0007_alter_validators_add_error_messages', '2018-04-16 19:46:06.322094');
INSERT INTO "django_migrations" VALUES (12, 'auth', '0008_alter_user_username_max_length', '2018-04-16 19:46:06.500605');
INSERT INTO "django_migrations" VALUES (13, 'teachers', '0001_initial', '2018-04-16 19:46:06.688601');
INSERT INTO "django_migrations" VALUES (14, 'representatives', '0001_initial', '2018-04-16 19:46:06.900799');
INSERT INTO "django_migrations" VALUES (15, 'students', '0001_initial', '2018-04-16 19:46:07.119571');
INSERT INTO "django_migrations" VALUES (16, 'classrooms', '0001_initial', '2018-04-16 19:46:07.366643');
INSERT INTO "django_migrations" VALUES (17, 'classes', '0001_initial', '2018-04-16 19:46:07.568143');
INSERT INTO "django_migrations" VALUES (18, 'exams', '0001_initial', '2018-04-16 19:46:07.758533');
INSERT INTO "django_migrations" VALUES (19, 'sessions', '0001_initial', '2018-04-16 19:46:07.960209');
INSERT INTO "django_migrations" VALUES (20, 'exams', '0002_examresult_grade', '2018-04-19 14:41:01.622359');

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS "django_session";
CREATE TABLE "django_session" (
  "session_key" varchar(40) NOT NULL,
  "session_data" text NOT NULL,
  "expire_date" datetime NOT NULL,
  PRIMARY KEY ("session_key")
);

-- ----------------------------
-- Records of django_session
-- ----------------------------
INSERT INTO "django_session" VALUES ('60uk1wdv09k7k0eqgdcf5psmd5nf924d', 'YTU2OTQzZjI5OGYzMWRkMDAyMWJmNzI4MDM3MDA1NzY1MWM4M2RmMjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI1NzkxMjdhMTdhMzJkMjVmMjIxYzE4MjYwMzA3ZGZmZjE2YzdlMDg4In0=', '2018-04-30 19:47:05.381958');

-- ----------------------------
-- Table structure for exams_exam
-- ----------------------------
DROP TABLE IF EXISTS "exams_exam";
CREATE TABLE "exams_exam" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "created_at" datetime NOT NULL,
  "updated_at" datetime,
  "name" varchar(100) NOT NULL,
  "date" datetime,
  "type" integer unsigned NOT NULL
);

-- ----------------------------
-- Records of exams_exam
-- ----------------------------
INSERT INTO "exams_exam" VALUES (1, '2018-04-19 14:58:07.423515', '2018-04-19 14:58:07.423580', 'Python', '2018-04-16 00:00:00', 2);
INSERT INTO "exams_exam" VALUES (2, '2018-04-19 18:56:37.478908', '2018-04-19 18:56:37.478994', 'PHP', '2018-04-19 00:00:00', 1);
INSERT INTO "exams_exam" VALUES (3, '2018-04-19 18:56:52.140923', '2018-04-19 18:56:52.140983', 'Java', '2018-04-17 00:00:00', 0);
INSERT INTO "exams_exam" VALUES (4, '2018-04-19 18:57:13.492441', '2018-04-19 18:57:13.492511', 'Rails', '2018-04-18 00:00:00', 2);
INSERT INTO "exams_exam" VALUES (5, '2018-04-19 19:13:01.033218', '2018-04-19 19:13:01.033283', '.Net', '2018-04-13 00:00:00', 1);
INSERT INTO "exams_exam" VALUES (6, '2018-04-19 19:44:32.459557', '2018-04-19 19:44:32.459610', 'Oracle', '2018-04-16 00:00:00', 0);
INSERT INTO "exams_exam" VALUES (7, '2018-04-19 19:44:41.613883', '2018-04-19 19:44:41.613973', 'Mysql', '2017-12-09 00:00:00', 0);
INSERT INTO "exams_exam" VALUES (8, '2018-04-19 19:44:51.285639', '2018-04-19 19:44:51.285706', 'Postgres', '2017-08-01 00:00:00', 0);
INSERT INTO "exams_exam" VALUES (9, '2018-04-19 19:45:02.426031', '2018-04-19 19:45:02.426112', 'MariaDB', NULL, 0);
INSERT INTO "exams_exam" VALUES (10, '2018-04-19 19:46:13.262381', '2018-04-19 19:46:13.262434', 'SQL Server', '2018-04-16 00:00:00', 0);

-- ----------------------------
-- Table structure for exams_examresult
-- ----------------------------
DROP TABLE IF EXISTS "exams_examresult";
CREATE TABLE "exams_examresult" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "created_at" datetime NOT NULL,
  "updated_at" datetime,
  "clas_id" integer NOT NULL,
  "exam_id" integer NOT NULL,
  "student_id" integer NOT NULL,
  "grade" real NOT NULL,
  FOREIGN KEY ("clas_id") REFERENCES "classes_class" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY ("exam_id") REFERENCES "exams_exam" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY ("student_id") REFERENCES "students_student" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
);

-- ----------------------------
-- Records of exams_examresult
-- ----------------------------
INSERT INTO "exams_examresult" VALUES (1, '2018-04-19 16:16:49.109709', '2018-04-19 16:49:40.723506', 1, 1, 1, 5.0);
INSERT INTO "exams_examresult" VALUES (2, '2018-04-19 19:10:21.910644', '2018-04-19 19:10:21.910694', 1, 2, 1, 4.5);
INSERT INTO "exams_examresult" VALUES (3, '2018-04-19 19:10:32.128913', '2018-04-19 19:10:32.128965', 1, 3, 1, 5.5);
INSERT INTO "exams_examresult" VALUES (4, '2018-04-19 19:10:43.087059', '2018-04-19 19:37:58.037065', 1, 4, 1, 4.0);
INSERT INTO "exams_examresult" VALUES (5, '2018-04-19 19:13:16.991238', '2018-04-19 19:13:16.991291', 1, 5, 1, 6.0);
INSERT INTO "exams_examresult" VALUES (6, '2018-04-19 19:46:34.530045', '2018-04-19 19:46:34.530092', 3, 6, 3, 4.0);
INSERT INTO "exams_examresult" VALUES (7, '2018-04-19 19:46:47.089549', '2018-04-19 19:46:47.089599', 3, 7, 3, 4.3);
INSERT INTO "exams_examresult" VALUES (8, '2018-04-19 19:46:56.889881', '2018-04-19 19:46:56.889936', 3, 8, 3, 4.5);
INSERT INTO "exams_examresult" VALUES (9, '2018-04-19 19:47:08.832464', '2018-04-19 19:47:08.832519', 3, 9, 3, 4.7);
INSERT INTO "exams_examresult" VALUES (10, '2018-04-19 19:47:25.373893', '2018-04-19 19:47:25.373942', 3, 10, 3, 4.9);
INSERT INTO "exams_examresult" VALUES (11, '2018-04-19 19:47:40.812084', '2018-04-19 19:47:40.812145', 3, 6, 1, 5.0);
INSERT INTO "exams_examresult" VALUES (12, '2018-04-19 19:47:55.809874', '2018-04-19 19:47:55.809924', 3, 7, 1, 5.3);
INSERT INTO "exams_examresult" VALUES (13, '2018-04-19 19:48:07.189274', '2018-04-19 19:48:07.189323', 3, 8, 1, 5.6);
INSERT INTO "exams_examresult" VALUES (14, '2018-04-19 19:48:16.981408', '2018-04-19 19:48:16.981458', 3, 9, 1, 5.9);
INSERT INTO "exams_examresult" VALUES (15, '2018-04-19 19:48:34.869440', '2018-04-19 19:48:34.869497', 3, 10, 1, 6.2);
INSERT INTO "exams_examresult" VALUES (16, '2018-04-19 19:56:30.448523', '2018-04-19 19:56:30.448573', 1, 1, 3, 3.5);
INSERT INTO "exams_examresult" VALUES (17, '2018-04-19 19:56:53.182519', '2018-04-19 19:56:53.182569', 1, 2, 3, 3.6);
INSERT INTO "exams_examresult" VALUES (18, '2018-04-19 19:57:34.267324', '2018-04-19 19:57:34.267373', 1, 3, 3, 3.4);
INSERT INTO "exams_examresult" VALUES (19, '2018-04-19 19:57:48.761569', '2018-04-19 19:57:48.761622', 1, 4, 3, 3.7);
INSERT INTO "exams_examresult" VALUES (20, '2018-04-19 19:58:04.325788', '2018-04-19 19:58:04.325851', 1, 5, 3, 3.9);

-- ----------------------------
-- Table structure for representatives_representative
-- ----------------------------
DROP TABLE IF EXISTS "representatives_representative";
CREATE TABLE "representatives_representative" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "created_at" datetime NOT NULL,
  "updated_at" datetime,
  "name" varchar(100) NOT NULL,
  "last_name" varchar(100) NOT NULL,
  "phone" varchar(15) NOT NULL,
  "mail" varchar(254) NOT NULL,
  "address" varchar(100) NOT NULL
);

-- ----------------------------
-- Records of representatives_representative
-- ----------------------------
INSERT INTO "representatives_representative" VALUES (1, '2018-04-17 20:53:42.527020', '2018-04-17 20:53:42.527075', 'Leonel', 'Torrejon', '+56963008577', 'leo.torrejon@gmail.com', 'Carmen 187');

-- ----------------------------
-- Table structure for sqlite_sequence
-- ----------------------------
DROP TABLE IF EXISTS "sqlite_sequence";
CREATE TABLE "sqlite_sequence" (
  "name",
  "seq"
);

-- ----------------------------
-- Records of sqlite_sequence
-- ----------------------------
INSERT INTO "sqlite_sequence" VALUES ('django_migrations', 20);
INSERT INTO "sqlite_sequence" VALUES ('django_admin_log', 26);
INSERT INTO "sqlite_sequence" VALUES ('django_content_type', 14);
INSERT INTO "sqlite_sequence" VALUES ('auth_permission', 42);
INSERT INTO "sqlite_sequence" VALUES ('auth_user', 1);
INSERT INTO "sqlite_sequence" VALUES ('representatives_representative', 1);
INSERT INTO "sqlite_sequence" VALUES ('students_student', 21);
INSERT INTO "sqlite_sequence" VALUES ('teachers_teacher', 2);
INSERT INTO "sqlite_sequence" VALUES ('classrooms_classroom', 2);
INSERT INTO "sqlite_sequence" VALUES ('classes_class', 3);
INSERT INTO "sqlite_sequence" VALUES ('exams_examresult', 20);
INSERT INTO "sqlite_sequence" VALUES ('exams_exam', 10);
INSERT INTO "sqlite_sequence" VALUES ('classes_studentclass', 2);

-- ----------------------------
-- Table structure for students_student
-- ----------------------------
DROP TABLE IF EXISTS "students_student";
CREATE TABLE "students_student" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "created_at" datetime NOT NULL,
  "updated_at" datetime,
  "name" varchar(100) NOT NULL,
  "last_name" varchar(100) NOT NULL,
  "phone" varchar(100) NOT NULL,
  "mail" varchar(254) NOT NULL,
  "representative_id" integer NOT NULL,
  FOREIGN KEY ("representative_id") REFERENCES "representatives_representative" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
);

-- ----------------------------
-- Records of students_student
-- ----------------------------
INSERT INTO "students_student" VALUES (1, '2018-04-17 21:02:41.521988', '2018-04-19 16:27:58.552497', 'Leonel', 'Torrejon', '+56963008577', 'leo.torrejon@gmail.com', 1);
INSERT INTO "students_student" VALUES (3, '2018-04-18 15:47:26.973441', '2018-04-18 15:47:26.973500', 'Jesus', 'Cardenas', '+56966667777', 'jesus@norte.com', 1);
INSERT INTO "students_student" VALUES (4, '2018-04-19 20:12:49.205081', '2018-04-19 20:12:49.205128', 'Felipe', 'Torrejon', '+56966667777', 'algo@algo.com', 1);
INSERT INTO "students_student" VALUES (5, '2018-04-19 20:12:58.820245', '2018-04-19 20:12:58.820293', 'Sofia', 'Torrejon', '+56966667777', 'algo@algo.com', 1);
INSERT INTO "students_student" VALUES (6, '2018-04-19 20:13:11.240592', '2018-04-19 20:13:11.240644', 'Angel', 'Torrejon', '+56966667777', 'algo@algo.com', 1);
INSERT INTO "students_student" VALUES (7, '2018-04-19 20:13:22.665651', '2018-04-19 20:13:22.665703', 'Horacio', 'Torrejon', '+56966667777', 'algo@algo.com', 1);
INSERT INTO "students_student" VALUES (8, '2018-04-19 20:13:48.798762', '2018-04-19 20:13:48.798806', 'Fabian', 'Torrejon', '+56966667777', 'algo@algo.com', 1);
INSERT INTO "students_student" VALUES (9, '2018-04-19 20:13:58.670664', '2018-04-19 20:13:58.670714', 'Claudio', 'Torrejon', '+56966667777', 'algo@algo.com', 1);
INSERT INTO "students_student" VALUES (10, '2018-04-19 20:14:06.710945', '2018-04-19 20:14:06.710994', 'Haylin', 'Torrejon', '+56966667777', 'algo@algo.com', 1);
INSERT INTO "students_student" VALUES (11, '2018-04-19 20:14:27.797046', '2018-04-19 20:14:27.797101', 'Nicolas', 'Torrejon', '+56966667777', 'algo@algo.com', 1);
INSERT INTO "students_student" VALUES (12, '2018-04-19 20:14:37.419317', '2018-04-19 20:14:37.419371', 'Humberto', 'Torrejon', '+56966667777', 'algo@algo.com', 1);
INSERT INTO "students_student" VALUES (13, '2018-04-19 20:14:49.841158', '2018-04-19 20:14:49.841211', 'Enrique', 'Torrejon', '+56966667777', 'algo@algo.com', 1);
INSERT INTO "students_student" VALUES (14, '2018-04-19 20:15:18.230904', '2018-04-19 20:15:18.230947', 'Nicole', 'Torrejon', '+56966667777', 'algo@algo.com', 1);
INSERT INTO "students_student" VALUES (15, '2018-04-19 20:15:27.426404', '2018-04-19 20:15:27.426455', 'Andrea', 'Torrejon', '+56966667777', 'algo@algo.com', 1);
INSERT INTO "students_student" VALUES (16, '2018-04-19 20:15:36.133254', '2018-04-19 20:15:36.133304', 'Noemi', 'Torrejon', '+56966667777', 'algo@algo.com', 1);
INSERT INTO "students_student" VALUES (17, '2018-04-19 20:15:43.792651', '2018-04-19 20:15:43.792697', 'Florentina', 'Torrejon', '+56966667777', 'algo@algo.com', 1);
INSERT INTO "students_student" VALUES (18, '2018-04-19 20:15:52.828570', '2018-04-19 20:15:52.828622', 'Carolina', 'Torrejon', '+56966667777', 'algo@algo.com', 1);
INSERT INTO "students_student" VALUES (19, '2018-04-19 20:16:14.616930', '2018-04-19 20:16:14.616983', 'Paola', 'Torrejon', '+56966667777', 'algo@algo.com', 1);
INSERT INTO "students_student" VALUES (20, '2018-04-19 20:16:41.161755', '2018-04-19 20:16:41.161798', 'Armando', 'Torrejon', '+56966667777', 'algo@algo.com', 1);
INSERT INTO "students_student" VALUES (21, '2018-04-19 20:16:51.558955', '2018-04-19 20:16:55.334515', 'Anakin', 'Torrejon', '+56966667777', 'algo@algo.com', 1);

-- ----------------------------
-- Table structure for teachers_teacher
-- ----------------------------
DROP TABLE IF EXISTS "teachers_teacher";
CREATE TABLE "teachers_teacher" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "created_at" datetime NOT NULL,
  "updated_at" datetime,
  "name" varchar(100) NOT NULL,
  "last_name" varchar(100) NOT NULL,
  "phone" varchar(15) NOT NULL,
  "mail" varchar(254) NOT NULL,
  "active" bool NOT NULL
);

-- ----------------------------
-- Records of teachers_teacher
-- ----------------------------
INSERT INTO "teachers_teacher" VALUES (1, '2018-04-19 02:18:18.282478', '2018-04-19 02:18:18.282527', 'Catherine', 'Muñoz', '+56966667777', 'cathmunozal@gmail.com', 1);
INSERT INTO "teachers_teacher" VALUES (2, '2018-04-19 04:38:05.543805', '2018-04-19 04:38:05.543852', 'Felipe', 'Sanhueza', '+56966667777', 'felipe@gmail.com', 1);

-- ----------------------------
-- Indexes structure for table auth_group_permissions
-- ----------------------------
CREATE INDEX "auth_group_permissions_group_id_b120cbf9"
ON "auth_group_permissions" (
  "group_id" ASC
);
CREATE UNIQUE INDEX "auth_group_permissions_group_id_permission_id_0cd325b0_uniq"
ON "auth_group_permissions" (
  "group_id" ASC,
  "permission_id" ASC
);
CREATE INDEX "auth_group_permissions_permission_id_84c5c92e"
ON "auth_group_permissions" (
  "permission_id" ASC
);

-- ----------------------------
-- Auto increment value for auth_permission
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 42 WHERE name = 'auth_permission';

-- ----------------------------
-- Indexes structure for table auth_permission
-- ----------------------------
CREATE INDEX "auth_permission_content_type_id_2f476e4b"
ON "auth_permission" (
  "content_type_id" ASC
);
CREATE UNIQUE INDEX "auth_permission_content_type_id_codename_01ab375a_uniq"
ON "auth_permission" (
  "content_type_id" ASC,
  "codename" ASC
);

-- ----------------------------
-- Auto increment value for auth_user
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 1 WHERE name = 'auth_user';

-- ----------------------------
-- Indexes structure for table auth_user_groups
-- ----------------------------
CREATE INDEX "auth_user_groups_group_id_97559544"
ON "auth_user_groups" (
  "group_id" ASC
);
CREATE INDEX "auth_user_groups_user_id_6a12ed8b"
ON "auth_user_groups" (
  "user_id" ASC
);
CREATE UNIQUE INDEX "auth_user_groups_user_id_group_id_94350c0c_uniq"
ON "auth_user_groups" (
  "user_id" ASC,
  "group_id" ASC
);

-- ----------------------------
-- Indexes structure for table auth_user_user_permissions
-- ----------------------------
CREATE INDEX "auth_user_user_permissions_permission_id_1fbb5f2c"
ON "auth_user_user_permissions" (
  "permission_id" ASC
);
CREATE INDEX "auth_user_user_permissions_user_id_a95ead1b"
ON "auth_user_user_permissions" (
  "user_id" ASC
);
CREATE UNIQUE INDEX "auth_user_user_permissions_user_id_permission_id_14a6b632_uniq"
ON "auth_user_user_permissions" (
  "user_id" ASC,
  "permission_id" ASC
);

-- ----------------------------
-- Auto increment value for classes_class
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 3 WHERE name = 'classes_class';

-- ----------------------------
-- Indexes structure for table classes_class
-- ----------------------------
CREATE INDEX "classes_class_classroom_id_f79bac87"
ON "classes_class" (
  "classroom_id" ASC
);
CREATE INDEX "classes_class_head_teacher_id_e9a0ec80"
ON "classes_class" (
  "head_teacher_id" ASC
);
CREATE INDEX "classes_class_teacher_id_e236cda2"
ON "classes_class" (
  "teacher_id" ASC
);

-- ----------------------------
-- Auto increment value for classes_studentclass
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 2 WHERE name = 'classes_studentclass';

-- ----------------------------
-- Indexes structure for table classes_studentclass
-- ----------------------------
CREATE INDEX "classes_studentclass_clas_id_f00cd770"
ON "classes_studentclass" (
  "clas_id" ASC
);
CREATE INDEX "classes_studentclass_student_id_24a10c2e"
ON "classes_studentclass" (
  "student_id" ASC
);

-- ----------------------------
-- Auto increment value for classrooms_classroom
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 2 WHERE name = 'classrooms_classroom';

-- ----------------------------
-- Auto increment value for django_admin_log
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 26 WHERE name = 'django_admin_log';

-- ----------------------------
-- Indexes structure for table django_admin_log
-- ----------------------------
CREATE INDEX "django_admin_log_content_type_id_c4bce8eb"
ON "django_admin_log" (
  "content_type_id" ASC
);
CREATE INDEX "django_admin_log_user_id_c564eba6"
ON "django_admin_log" (
  "user_id" ASC
);

-- ----------------------------
-- Auto increment value for django_content_type
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 14 WHERE name = 'django_content_type';

-- ----------------------------
-- Indexes structure for table django_content_type
-- ----------------------------
CREATE UNIQUE INDEX "django_content_type_app_label_model_76bd3d3b_uniq"
ON "django_content_type" (
  "app_label" ASC,
  "model" ASC
);

-- ----------------------------
-- Auto increment value for django_migrations
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 20 WHERE name = 'django_migrations';

-- ----------------------------
-- Indexes structure for table django_session
-- ----------------------------
CREATE INDEX "django_session_expire_date_a5c62663"
ON "django_session" (
  "expire_date" ASC
);

-- ----------------------------
-- Auto increment value for exams_exam
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 10 WHERE name = 'exams_exam';

-- ----------------------------
-- Auto increment value for exams_examresult
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 20 WHERE name = 'exams_examresult';

-- ----------------------------
-- Indexes structure for table exams_examresult
-- ----------------------------
CREATE INDEX "exams_examresult_clas_id_9db8d098"
ON "exams_examresult" (
  "clas_id" ASC
);
CREATE INDEX "exams_examresult_exam_id_d5908709"
ON "exams_examresult" (
  "exam_id" ASC
);
CREATE INDEX "exams_examresult_student_id_b018ef6b"
ON "exams_examresult" (
  "student_id" ASC
);

-- ----------------------------
-- Auto increment value for representatives_representative
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 1 WHERE name = 'representatives_representative';

-- ----------------------------
-- Auto increment value for students_student
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 21 WHERE name = 'students_student';

-- ----------------------------
-- Indexes structure for table students_student
-- ----------------------------
CREATE INDEX "students_student_representative_id_0de07c1a"
ON "students_student" (
  "representative_id" ASC
);

-- ----------------------------
-- Auto increment value for teachers_teacher
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 2 WHERE name = 'teachers_teacher';

PRAGMA foreign_keys = true;
