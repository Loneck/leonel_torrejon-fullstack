1.Escriba una Query que entregue la lista de alumnos para el curso "programación"

    SELECT
        classes_studentclass.student_id AS id,
        students_student.name AS name,
        students_student.last_name AS "last name",
        classes_class.name AS class 
    FROM
        classes_studentclass
        INNER JOIN students_student ON classes_studentclass.student_id = students_student.id
        INNER JOIN classes_class ON classes_studentclass.clas_id = classes_class.id 
    WHERE
        classes_studentclass.clas_id = classes_class.id 
        AND classes_class.name = 'Programacion'

    
2.Escriba una Query que calcule el promedio de notas de un alumno en un curso.

    SELECT
        Avg( exams_examresult.grade ) AS average,
        exams_examresult.clas_id AS class 
    FROM
        exams_examresult 
    WHERE
        exams_examresult.student_id = 1 
        AND exams_examresult.clas_id = 1


3.Escriba una Query que entregue a los alumnos y el promedio que tiene en cada curso.

    SELECT
        Avg( exams_examresult.grade ) AS average,
        classes_class.name AS clas,
        students_student.name 
    FROM
        exams_examresult
        INNER JOIN classes_class ON exams_examresult.clas_id = classes_class.id
        INNER JOIN students_student ON exams_examresult.student_id = students_student.id 
    GROUP BY
        exams_examresult.clas_id,
        exams_examresult.student_id


4.Escriba una Query que lista a todos los alumnos con más de un curso con promedio rojo.

    SELECT
        Avg( exams_examresult.grade ) AS average,
        classes_class.name AS clas,
        students_student.name 
    FROM
        exams_examresult
        INNER JOIN classes_class ON exams_examresult.clas_id = classes_class.id
        INNER JOIN students_student ON exams_examresult.student_id = students_student.id 
    WHERE
        exams_examresult.grade <= 3.9 
    GROUP BY
        exams_examresult.clas_id,
        exams_examresult.student_id


5.Dejando de lado el problema del cólegio se tiene una tabla con información de jugadores de tenis:
PLAYERS(Nombre, Pais, Ranking). Suponga que Ranking es un número de 1 a 100 que es distinto para cada jugador.
Si la tabla en un momento dado tiene solo 20 registros, indique cuantos registros tiene la tabla que resulta de la siguiente consulta:

SELECT c1.Nombre, c2.Nombre
FROM PLAYERS c1, PLAYERS c2
WHERE c1.Ranking > c2.Ranking

Seleccione las respuestas correctas:
a) 400
b) 190
c) 20
d) imposible saberlo

La respuesta correcta es la alternativa b) 190, lo comprobre utilizando la tabla de estudiantes de la bbdd de colegio con la siguiente query

    SELECT
        c1.name AS name1,
        c2.name AS name2 
    FROM
        students_student AS c1,
        students_student AS c2 
    WHERE
        c1.id > c2.id