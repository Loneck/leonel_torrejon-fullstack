# Test


## Modelo de datos

Imagen adjunta llamada bd_clase.jpg y archivo original creado con star uml bd_clase.mdj


## SQL

Respuestas en archivo SQL.txt


## Diseño de software


### Backend

Pasos para correr el proyecto

- Tener instalado Python3 y pip
- Crear ambiente virtual (python3 -m venv 'nombre a eleccion')
- Activar ambiente virtual (source 'nombre a eleccion'/bin/activate)
- Instalar Django y librerias con el comando pip install -r requirements.txt
- Realizar por cada app su migracion (./manage.py makemigrations 'nombre de la app')
- Migrar la base de datos (./manage.py migrate)
- Levantar el proyecto (./manage.py runserver 0.0.0.0:8000) 


* Datos de prueba en archivo school.sql
